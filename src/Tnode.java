import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private static List<String> acceptedOperators = new ArrayList<String>()
   {
      {
         add("+");
         add("-");
         add("*");
         add("/");
         add("ROT");
         add("SWAP");
      }
   };

   public static void main (String[] param) {
      String rpn = "2 5 SWAP -";
      String rpn2 = "2 5 9 ROT - +";
      String rpn3 = "2 5 9 ROT + SWAP -";

      Tnode res = buildFromRPN (rpn);
      Tnode res2 = buildFromRPN(rpn2);
      Tnode res3 = buildFromRPN(rpn3);
      System.out.println ("Tree: " + res);
      System.out.println ("Tree: " + res2);
      System.out.println("Tree: " + res3);
   }

   public Tnode(String name)
   {
      this.name = name;
   }
   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      if(firstChild != null){
         b.append("(");
         b.append(firstChild.toString());
      }

      if(nextSibling != null){
         b.append(",");
         b.append(nextSibling.toString());
      }

      if(isParentNode())
         b.append(")");

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Tnode root;
      String trimmedEquation = pol.trim();
      if (trimmedEquation.length() == 0)
         throw new RuntimeException("Given equation is empty!");


      if(trimmedEquation.length() == 1 && isNumeric(trimmedEquation))
         return new Tnode(trimmedEquation);

      String[] splitterEquation = trimmedEquation.split("\\s+");
      Stack<Tnode> nodes = new Stack<Tnode>();

      for (String currentElement : splitterEquation) {

            if(currentElement.equals("SWAP")){
               if(nodes.size() < 2)
                  throw new RuntimeException(String.format("Not enough elements to perform SWAP in equation %s", trimmedEquation));
               Tnode a = nodes.get(nodes.size()-1);
               Tnode b = nodes.get(nodes.size()-2);

               nodes.removeAllElements();
               nodes.add(a);
               nodes.add(b);

               Stack<Tnode> tmp = (Stack)nodes.clone();
               tmp.removeElementAt(nodes.size()-1);
               tmp.removeElementAt(nodes.size()-2);

               for(Tnode node : tmp){

                  nodes.add(node);
               }

               continue;
            }
            if(currentElement.equals("ROT")){
               if(nodes.size() < 3)
                  throw new RuntimeException(String.format("Not enough elements to perform ROT in equation %s", trimmedEquation));
               Tnode a = nodes.get(nodes.size()-1);
               Tnode b = nodes.get(nodes.size()-2);
               Tnode c = nodes.get(nodes.size()-3);

               nodes.removeAllElements();
               nodes.add(b);
               nodes.add(a);
               nodes.add(c);

               Stack<Tnode> tmp = (Stack)nodes.clone();
               tmp.removeElementAt(nodes.size()-1);
               tmp.removeElementAt(nodes.size()-2);
               tmp.removeElementAt(nodes.size()-3);

               for(Tnode node : tmp){

                  nodes.add(node);
               }

               continue;

            }
            Tnode node = new Tnode(currentElement);
            if (!isNumeric(currentElement)) {
               if (!acceptedOperators.contains(currentElement))
                  throw new RuntimeException(String.format("%s is not on the list of accepted operators [%s] in equation %s", currentElement, acceptedOperators, trimmedEquation));

               if(nodes.size() < 2)
                  throw new RuntimeException(String.format("Not enough numbers in equation  %s", trimmedEquation));
               node.nextSibling = nodes.pop();
               node.firstChild = nodes.pop();
            }
            nodes.push(node);

      }

      root = nodes.peek();
      if(isNumeric(root.name))
         throw new RuntimeException(String.format("Too many numbers in equation  %s", trimmedEquation));

      return nodes.pop();
   }

   public boolean isParentNode(){
      return (nextSibling != null && firstChild != null);
   }

   public static boolean isNumeric(String str) {

      return str.matches("-?\\d+");
   }

}

